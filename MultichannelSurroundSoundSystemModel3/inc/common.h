#include <stdfix.h>

#define SAMPLE_RATE 44000
#define BLOCK_SIZE 16
#define MAX_NUM_CHANNEL 6

typedef short DSPshort;					/* DSP integer */
typedef unsigned short DSPushort;		/* DSP unsigned integer */
typedef int DSPint;						/* native integer */
typedef fract DSPfract;				/* DSP fixed-point fractional */
typedef double DSPaccum;				/* DSP fixed-point fractional */
