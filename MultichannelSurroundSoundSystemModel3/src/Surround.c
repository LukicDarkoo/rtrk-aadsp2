#include "Surround.h"
#include <stdfix.h>

__memX struct _Surround surround;
__memY DSPfract __attribute__((__aligned__(64))) leftCircularBuffer[64];
__memY DSPfract __attribute__((__aligned__(64))) rightCircularBuffer[64];

void Surround_Init(
	DSPfract inputGain, DSPfract headroomGain, SurroundMode mode,
	__memY DSPfract* inputLeftBuffer,
	__memY DSPfract* inputRightBuffer,
	__memY DSPfract* outputLsBuffer,
	__memY DSPfract* outputLeftBuffer,
	__memY DSPfract* outputCenterBuffer,
	__memY DSPfract* outputRightBuffer,
	__memY DSPfract* outputRsBuffer,
	__memY DSPfract* outputLfeBuffer
) {
	DSPint i;

	surround.inputLeftBuffer = inputLeftBuffer;
	surround.inputRightBuffer = inputRightBuffer;
	surround.outputLsBuffer = outputLsBuffer;
	surround.outputLeftBuffer = outputLeftBuffer;
	surround.outputCenterBuffer = outputCenterBuffer;
	surround.outputRightBuffer = outputRightBuffer;
	surround.outputRsBuffer = outputRsBuffer;
	surround.outputLfeBuffer = outputLfeBuffer;

	surround.inputLeftGain = inputGain;					// -7dB (0.44668359215096315)
	surround.inputRightGain = inputGain;				// -7dB
	surround.headroomGain = headroomGain;				// -2dB (0.79432823472428149)
	surround.outputLsGain = 0.50118723362727224r;		// -6dB
	surround.outputLeftGain = 0.89125093813374556r;		// -1dB (0.89125093813374556)
	surround.outputRightGain = 0.89125093813374556r;	// -1dB
	surround.outputRsGain = 0.50118723362727224r;		// -6dB (0.50118723362727224)

	surround.circularBufferReadIndex = 0;
	surround.circularBufferWriteIndex = SURROUND_DELAY_SAMPLES - 1;

	for (i = 0; i < SURROUND_CIRC_BUFF_SIZE; i++) {
		surround.leftCircularBuffer[i] = 0;
		surround.rightCircularBuffer[i] = 0;
	}

	Surround_SetMode(mode);
}


void Surround_SetMode(SurroundMode mode) {
	switch(mode) {
	case SURROUND_MODE_2_0_0:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 0;
		surround.enableCenter = 0;
		surround.enableLeftRight = 1;
		surround.enableLfe = 0;
		break;

	case SURROUND_MODE_2_0_1:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 0;
		surround.enableCenter = 0;
		surround.enableLeftRight = 1;
		surround.enableLfe = 1;
		break;

	case SURROUND_MODE_0_2_0:
		surround.enableMiddleCenter = 0;
		surround.enableLsRs = 1;
		surround.enableCenter = 0;
		surround.enableLeftRight = 0;
		surround.enableLfe = 0;
		break;

	case SURROUND_MODE_0_2_1:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 1;
		surround.enableCenter = 0;
		surround.enableLeftRight = 0;
		surround.enableLfe = 1;
		break;

	case SURROUND_MODE_3_2_0:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 1;
		surround.enableCenter = 1;
		surround.enableLeftRight = 1;
		surround.enableLfe = 0;
		break;
	}
}

/*
void Surround_Process() {
		DSPint i;

		DSPfract middleStageCenter;
		DSPfract middleStageLeft;
		DSPfract middleStageRight;
		DSPfract inputStageLeft;
		DSPfract inputStageRight;

		__memY DSPfract *inputLeftBuffer = surround.inputLeftBuffer;
		__memY DSPfract *inputRightBuffer = surround.inputRightBuffer;
		__memY DSPfract *outputLsBuffer = surround.outputLsBuffer;
		__memY DSPfract *outputLeftBuffer = surround.outputLeftBuffer;
		__memY DSPfract *outputCenterBuffer = surround.outputCenterBuffer;
		__memY DSPfract *outputLfeBuffer = surround.outputLfeBuffer;
		__memY DSPfract *outputRightBuffer = surround.outputRightBuffer;
		__memY DSPfract *outputRsBuffer = surround.outputRsBuffer;


		for (i = 0; i < BLOCK_SIZE; i++) {
			// Input stage
			inputStageLeft = *inputLeftBuffer * surround.inputLeftGain;
			inputStageRight = *inputRightBuffer * surround.inputRightGain;


			if (surround.enableMiddleCenter == 1) {
				// Middle stage
				middleStageCenter = (inputStageLeft + inputStageRight) * surround.headroomGain;


				// Output stage
				if (surround.enableLeftRight == 1) {
					*outputLeftBuffer++ = middleStageCenter * surround.outputLeftGain;
					*outputRightBuffer++ = middleStageCenter * surround.outputRightGain;
				}

				if (surround.enableCenter == 1) {
					*outputCenterBuffer++ = middleStageCenter;
				}

				if (surround.enableLfe == 1) {
					*outputLfeBuffer++ = middleStageCenter;
				}
			}

			if (surround.enableLsRs == 1) {
				// Middle stage
				middleStageLeft = (surround.leftCircularBuffer[surround.circularBufferReadIndex] * surround.outputLsGain);
				middleStageRight = (surround.rightCircularBuffer[surround.circularBufferReadIndex] * surround.outputRsGain);

				surround.leftCircularBuffer[surround.circularBufferWriteIndex] = middleStageLeft + inputStageLeft;
				surround.rightCircularBuffer[surround.circularBufferWriteIndex] = middleStageRight + inputStageRight;

				surround.circularBufferWriteIndex = (surround.circularBufferWriteIndex + 1) % SURROUND_DELAY_SAMPLES;
				surround.circularBufferReadIndex = (surround.circularBufferReadIndex + 1) % SURROUND_DELAY_SAMPLES;

				// Output stage
				*outputLsBuffer++ = middleStageLeft;
				*outputRsBuffer++ = middleStageRight;
			}

			if (surround.enableLeftRight == 0) {
				*outputLeftBuffer++ = 0.0r;
				*outputRightBuffer++ = 0.0r;
			}

			inputLeftBuffer++;
			inputRightBuffer++;
		}
}
*/
