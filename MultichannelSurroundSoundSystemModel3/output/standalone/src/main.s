	.public _sampleBuffer
	.extern _Surround_Init
	.extern _Surround_Process
	.extern _cl_wavread_bits_per_sample
	.extern _cl_wavread_close
	.extern _cl_wavread_frame_rate
	.extern _cl_wavread_getnchannels
	.extern _cl_wavread_number_of_frames
	.extern _cl_wavread_open
	.extern _cl_wavread_recvsample
	.extern _cl_wavwrite_close
	.extern _cl_wavwrite_open
	.extern _cl_wavwrite_sendsample
	.public _main
	.extern _memset
	.extern _printf
	.extern _strcpy
	.extern __div
	.xdata_ovly
__extractedConst_0_1
	.dw  (0x392ced8e)
	.xdata_ovly
__extractedConst_1_1
	.dw  (0x65ac8c2f)
	.ydata_ovly
_sampleBuffer
	.bss (0x60)
	.xdata_ovly
_string_const_0
	.dw  (0x45)
	.dw  (0x72)
	.dw  (0x72)
	.dw  (0x6f)
	.dw  (0x72)
	.dw  (0x3a)
	.dw  (0x20)
	.dw  (0x43)
	.dw  (0x6f)
	.dw  (0x75)
	.dw  (0x6c)
	.dw  (0x64)
	.dw  (0x20)
	.dw  (0x6e)
	.dw  (0x6f)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x6f)
	.dw  (0x70)
	.dw  (0x65)
	.dw  (0x6e)
	.dw  (0x20)
	.dw  (0x77)
	.dw  (0x61)
	.dw  (0x76)
	.dw  (0x65)
	.dw  (0x66)
	.dw  (0x69)
	.dw  (0x6c)
	.dw  (0x65)
	.dw  (0x2e)
	.dw  (0xa)
	.dw  (0x0)
	.xdata_ovly
_string_const_1
	.dw  (0x45)
	.dw  (0x72)
	.dw  (0x72)
	.dw  (0x6f)
	.dw  (0x72)
	.dw  (0x3a)
	.dw  (0x20)
	.dw  (0x43)
	.dw  (0x6f)
	.dw  (0x75)
	.dw  (0x6c)
	.dw  (0x64)
	.dw  (0x20)
	.dw  (0x6e)
	.dw  (0x6f)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x6f)
	.dw  (0x70)
	.dw  (0x65)
	.dw  (0x6e)
	.dw  (0x20)
	.dw  (0x77)
	.dw  (0x61)
	.dw  (0x76)
	.dw  (0x65)
	.dw  (0x66)
	.dw  (0x69)
	.dw  (0x6c)
	.dw  (0x65)
	.dw  (0x2e)
	.dw  (0xa)
	.dw  (0x0)
	.code_ovly



	# This construction should ensure linking of crt0 in case when target is a standalone program without the OS
	.if defined(_OVLY_)
		.if .strcmp('standalone',_OVLY_)=0
		.if .strcmp('crystal32',_TARGET_FAMILY_)=0
			.extern __start         # dummy use of __start to force linkage of crt0
dummy		.equ(__start)
		.else
			.extern __intvec         # dummy use of __intvec to force linkage of intvec
dummy		.equ(__intvec)
		.endif
		.endif
	.endif

_main:			/* LN: 12 | CYCLE: 0 | RULES: () */ 
	xmem[i7] = i7			# LN: 12 | 
	i7 += 1			# LN: 12 | 
	xmem[i7] = i2; i7 += 1			# LN: 12, 12 | 
	xmem[i7] = i3; i7 += 1			# LN: 12, 12 | 
	xmem[i7] = i6; i7 += 1			# LN: 12, 12 | 
	i7 = i7 + (0x212)			# LN: 12 | 
	i1 = i7 - (0x1)			# LN: 12 | 
	xmem[i1] = a0h			# LN: 12 | 
	i1 = i7 - (0x2)			# LN: 12 | 
	xmem[i1] = i0			# LN: 12 | 
cline_12_0:			/* LN: 30 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 30 | 
	a0 = 0			# LN: 30 | 
	xmem[i0] = a0h			# LN: 30 | 
	do (0x6), label_end_92			# LN: 30 | 
cline_30_0:			/* LN: 31 | CYCLE: 0 | RULES: () */ 
label_begin_92:			/* LN: 30 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 31 | 
	a1 = xmem[i0]; a0 = 0			# LN: 31, 31 | 
	a1 = a1 << 4			# LN: 31 | 
	i0 = a1			# LN: 31 | 
	uhalfword(a1) = (0x10)			# LN: 31 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 31 | 
	call (_memset)			# LN: 31 | 
cline_31_0:			/* LN: 30 | CYCLE: 0 | RULES: () */ 
init_latch_label_0:			/* LN: 31 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 30 | 
	a0 = xmem[i0]			# LN: 30 | 
	uhalfword(a1) = (0x1)			# LN: 30 | 
	a0 = a0 + a1			# LN: 30 | 
	i0 = i7 - (0x3)			# LN: 30 | 
label_end_92:			# LN: 30 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 30 | 
cline_30_1:			/* LN: 35 | CYCLE: 0 | RULES: () */ 
for_end_0:			/* LN: 30 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 35 | 
	i0 = xmem[i0]			# LN: 35 | 
	i1 = i7 - (259 - 0)			# LN: 35 | 
	i4 = xmem[i0]			# LN: 35 | 
	i0 = i1			# LN: 35 | 
	i1 = i4			# LN: 35 | 
	call (_strcpy)			# LN: 35 | 
cline_35_0:			/* LN: 36 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (259 - 0)			# LN: 36 | 
	call (_cl_wavread_open)			# LN: 36 | 
	AnyReg(i0, a0h)			# LN: 36 | 
	i1 = i7 - (0x104)			# LN: 36 | 
	xmem[i1] = i0			# LN: 36 | 
cline_36_0:			/* LN: 37 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 37 | 
	a0 = xmem[i0]			# LN: 37 | 
	a0 & a0			# LN: 37 | 
	if (a != 0) jmp (else_0)			# LN: 37 | 
cline_37_0:			/* LN: 39 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_string_const_0)			# LN: 39 | 
	call (_printf)			# LN: 39 | 
cline_39_0:			/* LN: 40 | CYCLE: 0 | RULES: () */ 
	halfword(a0) = (0xffff)			# LN: 40 | 
	jmp (__epilogue_242)			# LN: 40 | 
cline_40_0:			/* LN: 46 | CYCLE: 0 | RULES: () */ 
endif_0:			/* LN: 37 | CYCLE: 0 | RULES: () */ 
else_0:			/* LN: 37 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 46 | 
	i0 = xmem[i0]			# LN: 46 | 
	call (_cl_wavread_getnchannels)			# LN: 46 | 
	i0 = i7 - (0x105)			# LN: 46 | 
	xmem[i0] = a0h			# LN: 46 | 
cline_46_0:			/* LN: 47 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 47 | 
	i0 = xmem[i0]			# LN: 47 | 
	call (_cl_wavread_bits_per_sample)			# LN: 47 | 
	i0 = i7 - (0x106)			# LN: 47 | 
	xmem[i0] = a0h			# LN: 47 | 
cline_47_0:			/* LN: 48 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 48 | 
	i0 = xmem[i0]			# LN: 48 | 
	call (_cl_wavread_frame_rate)			# LN: 48 | 
	i0 = i7 - (0x107)			# LN: 48 | 
	xmem[i0] = a0h			# LN: 48 | 
cline_48_0:			/* LN: 49 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 49 | 
	i0 = xmem[i0]			# LN: 49 | 
	call (_cl_wavread_number_of_frames)			# LN: 49 | 
	i0 = i7 - (0x108)			# LN: 49 | 
	xmem[i0] = a0h			# LN: 49 | 
cline_49_0:			/* LN: 54 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 54 | 
	i0 = xmem[i0]			# LN: 54 | 
	i1 = i7 - (520 - 0)			# LN: 54 | 
	i0 += 1			# LN: 54 | 
	i4 = xmem[i0]			# LN: 54 | 
	i0 = i1			# LN: 54 | 
	i1 = i4			# LN: 54 | 
	call (_strcpy)			# LN: 54 | 
cline_54_0:			/* LN: 55 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (520 - 0)			# LN: 55 | 
	i1 = i7 - (0x106)			# LN: 55 | 
	a0 = xmem[i1]			# LN: 55 | 
	uhalfword(a1) = (0x6)			# LN: 55 | 
	i1 = i7 - (0x107)			# LN: 55 | 
	b0 = xmem[i1]			# LN: 55 | 
	call (_cl_wavwrite_open)			# LN: 55 | 
	AnyReg(i0, a0h)			# LN: 55 | 
	i1 = i7 - (0x209)			# LN: 55 | 
	xmem[i1] = i0			# LN: 55 | 
cline_55_0:			/* LN: 56 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 56 | 
	a0 = xmem[i0]			# LN: 56 | 
	a0 & a0			# LN: 56 | 
	if (a != 0) jmp (else_1)			# LN: 56 | 
cline_56_0:			/* LN: 58 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_string_const_1)			# LN: 58 | 
	call (_printf)			# LN: 58 | 
cline_58_0:			/* LN: 59 | CYCLE: 0 | RULES: () */ 
	halfword(a0) = (0xffff)			# LN: 59 | 
	jmp (__epilogue_242)			# LN: 59 | 
cline_59_0:			/* LN: 62 | CYCLE: 0 | RULES: () */ 
switch_0:			/* LN: 62 | CYCLE: 0 | RULES: () */ 
endif_1:			/* LN: 56 | CYCLE: 0 | RULES: () */ 
else_1:			/* LN: 56 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 62 | 
	i0 = xmem[i0]			# LN: 62 | 
	uhalfword(a0) = (0x30)			# LN: 62 | 
	i0 += 2			# LN: 62 | 
	i0 = xmem[i0]			# LN: 62 | 
	uhalfword(a1) = (0x7)			# LN: 62 | 
	i0 += 1			# LN: 62 | 
	b0 = xmem[i0]			# LN: 62 | 
	a0 = a0 - b0			# LN: 62 | 
	a0 =- a0			# LN: 62 | 
	a0g = (0x0)			# LN: 62 | 
	a1g = (0x0)			# LN: 62 | 
	a0 - a1			# LN: 62 | 
	if (a == 0) jmp (case_0)			# LN: 62 | 
	jmp (switch_end_0)			# LN: 62 | 
cline_62_0:			/* LN: 64 | CYCLE: 0 | RULES: () */ 
case_0:			/* LN: 63 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[__extractedConst_0_1 + 0]			# LN: 64 | 
	i0 = i7 - (0x20a)			# LN: 64 | 
	xmem[i0] = a0h			# LN: 64 | 
cline_64_0:			/* LN: 65 | CYCLE: 0 | RULES: () */ 
	jmp (switch_end_0)			# LN: 65 | 
cline_65_0:			/* LN: 68 | CYCLE: 0 | RULES: () */ 
switch_1:			/* LN: 68 | CYCLE: 0 | RULES: () */ 
switch_end_0:			/* LN: 62 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 68 | 
	i0 = xmem[i0]			# LN: 68 | 
	uhalfword(a0) = (0x30)			# LN: 68 | 
	i0 = i0 + (0x3)			# LN: 68 | 
	i0 = xmem[i0]			# LN: 68 | 
	uhalfword(a1) = (0x2)			# LN: 68 | 
	i0 += 1			# LN: 68 | 
	b0 = xmem[i0]			# LN: 68 | 
	a0 = a0 - b0			# LN: 68 | 
	a0 =- a0			# LN: 68 | 
	a0g = (0x0)			# LN: 68 | 
	a1g = (0x0)			# LN: 68 | 
	a0 - a1			# LN: 68 | 
	if (a == 0) jmp (case_1)			# LN: 68 | 
	jmp (switch_end_1)			# LN: 68 | 
cline_68_0:			/* LN: 70 | CYCLE: 0 | RULES: () */ 
case_1:			/* LN: 69 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[__extractedConst_1_1 + 0]			# LN: 70 | 
	i0 = i7 - (0x20b)			# LN: 70 | 
	xmem[i0] = a0h			# LN: 70 | 
cline_70_0:			/* LN: 71 | CYCLE: 0 | RULES: () */ 
	jmp (switch_end_1)			# LN: 71 | 
cline_71_0:			/* LN: 79 | CYCLE: 0 | RULES: () */ 
switch_end_1:			/* LN: 68 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 79 | 
	i0 = xmem[i0]			# LN: 79 | 
	uhalfword(a0) = (0x30)			# LN: 79 | 
	i0 = i0 + (0x4)			# LN: 79 | 
	i0 = xmem[i0]			# LN: 79 | 
	i1 = (0) + (_sampleBuffer + 0)			# LN: 79 | 
	a1 = xmem[i0]			# LN: 79 | 
	a0 = a1 - a0			# LN: 79 | 
	i4 = (0) + (_sampleBuffer + 16)			# LN: 79 | 
	i5 = (0) + (_sampleBuffer + 48)			# LN: 79 | 
	i2 = (0) + (_sampleBuffer + 0)			# LN: 79 | 
	i3 = (0) + (_sampleBuffer + 32)			# LN: 79 | 
	i0 = (0) + (_sampleBuffer + 16)			# LN: 79 | 
	i6 = i7 - (0x212)			# LN: 79 | 
	xmem[i6] = i0			# LN: 79 | 
	i0 = (0) + (_sampleBuffer + 64)			# LN: 79 | 
	i6 = i7 - (0x211)			# LN: 79 | 
	xmem[i6] = i0			# LN: 79 | 
	i0 = (0) + (_sampleBuffer + 80)			# LN: 79 | 
	i6 = i7 - (0x210)			# LN: 79 | 
	xmem[i6] = i0			# LN: 79 | 
	i0 = i7 - (0x20a)			# LN: 79 | 
	a0 = xmem[i0]; b0 =+ a0			# LN: 79, 79 | 
	i0 = i7 - (0x20b)			# LN: 79 | 
	a1 = xmem[i0]			# LN: 79 | 
	i0 = i1			# LN: 79 | 
	i1 = i4			# LN: 79 | 
	i4 = i5			# LN: 79 | 
	i5 = i2			# LN: 79 | 
	i2 = i7 + (0x3)			# LN: 79 | 
	xmem[i2] = i3			# LN: 79 | 
	i2 = i7 + (0x2)			# LN: 79 | 
	i3 = i7 - (0x212)			# LN: 79 | 
	i3 = xmem[i3]			# LN: 79 | 
	xmem[i2] = i3			# LN: 79 | 
	i2 = i7 + (0x1)			# LN: 79 | 
	i3 = i7 - (0x211)			# LN: 79 | 
	i3 = xmem[i3]			# LN: 79 | 
	xmem[i2] = i3			# LN: 79 | 
	i2 = i7 - (0x0)			# LN: 79 | 
	i3 = i7 - (0x210)			# LN: 79 | 
	i3 = xmem[i3]			# LN: 79 | 
	xmem[i2] = i3			# LN: 79 | 
	i7 = i7 + (0x4)			# LN: 79 | 
	call (_Surround_Init)			# LN: 79 | 
	i7 = i7 - (0x4)			# LN: 79 | 
cline_79_0:			/* LN: 92 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20c)			# LN: 92 | 
	a0 = 0			# LN: 92 | 
	xmem[i0] = a0h			# LN: 92 | 
for_1:			/* LN: 92 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x108)			# LN: 92 | 
	a0 = xmem[i0]			# LN: 92 | 
	uhalfword(a1) = (0x10)			# LN: 92 | 
	call (__div)			# LN: 92 | 
	i0 = i7 - (0x20c)			# LN: 92 | 
	a1 = xmem[i0]			# LN: 92 | 
	a1 - a0			# LN: 92 | 
	if (a >= 0) jmp (for_end_1)			# LN: 92 | 
cline_92_0:			/* LN: 94 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 94 | 
	a0 = 0			# LN: 94 | 
	xmem[i0] = a0h			# LN: 94 | 
	do (0x10), label_end_93			# LN: 94 | 
cline_94_0:			/* LN: 96 | CYCLE: 0 | RULES: () */ 
label_begin_93:			/* LN: 94 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 96 | 
	a0 = 0			# LN: 96 | 
	xmem[i0] = a0h			# LN: 96 | 
for_3:			/* LN: 96 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 96 | 
	a0 = xmem[i0]			# LN: 96 | 
	i0 = i7 - (0x105)			# LN: 96 | 
	a1 = xmem[i0]			# LN: 96 | 
	a0 - a1			# LN: 96 | 
	if (a >= 0) jmp (for_end_3)			# LN: 96 | 
cline_96_0:			/* LN: 98 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 98 | 
	i0 = xmem[i0]			# LN: 98 | 
	call (_cl_wavread_recvsample)			# LN: 98 | 
	i0 = i7 - (0x20f)			# LN: 98 | 
	xmem[i0] = a0h			# LN: 98 | 
cline_98_0:			/* LN: 99 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 99 | 
	a0 = xmem[i0]			# LN: 99 | 
	a0 = a0 << 4			# LN: 99 | 
	i0 = a0			# LN: 99 | 
	i1 = i7 - (0x20d)			# LN: 99 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 99 | 
	a0 = xmem[i1]			# LN: 99 | 
	a1 = i0			# LN: 99 | 
	a0 = a1 + a0			# LN: 99 | 
	AnyReg(i0, a0h)			# LN: 99 | 
	i1 = i7 - (0x20f)			# LN: 99 | 
	a0 = xmem[i1]			# LN: 99 | 
	ymem[i0] = a0h			# LN: 99 | 
cline_99_0:			/* LN: 96 | CYCLE: 0 | RULES: () */ 
init_latch_label_1:			/* LN: 100 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 96 | 
	a0 = xmem[i0]			# LN: 96 | 
	uhalfword(a1) = (0x1)			# LN: 96 | 
	a0 = a0 + a1			# LN: 96 | 
	i0 = i7 - (0x20e)			# LN: 96 | 
	xmem[i0] = a0h			# LN: 96 | 
	jmp (for_3)			# LN: 96 | 
cline_96_1:			/* LN: 94 | CYCLE: 0 | RULES: () */ 
init_latch_label_2:			/* LN: 101 | CYCLE: 0 | RULES: () */ 
for_end_3:			/* LN: 96 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 94 | 
	a0 = xmem[i0]			# LN: 94 | 
	uhalfword(a1) = (0x1)			# LN: 94 | 
	a0 = a0 + a1			# LN: 94 | 
	i0 = i7 - (0x20d)			# LN: 94 | 
label_end_93:			# LN: 94 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 94 | 
cline_94_1:			/* LN: 103 | CYCLE: 0 | RULES: () */ 
for_end_2:			/* LN: 94 | CYCLE: 0 | RULES: () */ 
	call (_Surround_Process)			# LN: 103 | 
cline_103_0:			/* LN: 105 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 105 | 
	a0 = 0			# LN: 105 | 
	xmem[i0] = a0h			# LN: 105 | 
	do (0x10), label_end_95			# LN: 105 | 
cline_105_0:			/* LN: 107 | CYCLE: 0 | RULES: () */ 
label_begin_95:			/* LN: 105 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 107 | 
	a0 = 0			# LN: 107 | 
	xmem[i0] = a0h			# LN: 107 | 
	do (0x6), label_end_94			# LN: 107 | 
cline_107_0:			/* LN: 109 | CYCLE: 0 | RULES: () */ 
label_begin_94:			/* LN: 107 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 109 | 
	a0 = xmem[i0]			# LN: 109 | 
	a0 = a0 << 4			# LN: 109 | 
	i0 = a0			# LN: 109 | 
	i1 = i7 - (0x20d)			# LN: 109 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 109 | 
	a0 = xmem[i1]			# LN: 109 | 
	a1 = i0			# LN: 109 | 
	a0 = a1 + a0			# LN: 109 | 
	AnyReg(i0, a0h)			# LN: 109 | 
	i1 = i7 - (0x20f)			# LN: 109 | 
	a0 = ymem[i0]			# LN: 109 | 
	xmem[i1] = a0h			# LN: 109 | 
cline_109_0:			/* LN: 110 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 110 | 
	i1 = i7 - (0x20f)			# LN: 110 | 
	a0 = xmem[i1]			# LN: 110 | 
	i0 = xmem[i0]			# LN: 110 | 
	call (_cl_wavwrite_sendsample)			# LN: 110 | 
cline_110_0:			/* LN: 107 | CYCLE: 0 | RULES: () */ 
init_latch_label_3:			/* LN: 111 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 107 | 
	a0 = xmem[i0]			# LN: 107 | 
	uhalfword(a1) = (0x1)			# LN: 107 | 
	a0 = a0 + a1			# LN: 107 | 
	i0 = i7 - (0x20e)			# LN: 107 | 
label_end_94:			# LN: 107 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 107 | 
cline_107_1:			/* LN: 105 | CYCLE: 0 | RULES: () */ 
init_latch_label_4:			/* LN: 112 | CYCLE: 0 | RULES: () */ 
for_end_5:			/* LN: 107 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 105 | 
	a0 = xmem[i0]			# LN: 105 | 
	uhalfword(a1) = (0x1)			# LN: 105 | 
	a0 = a0 + a1			# LN: 105 | 
	i0 = i7 - (0x20d)			# LN: 105 | 
label_end_95:			# LN: 105 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 105 | 
cline_105_1:			/* LN: 92 | CYCLE: 0 | RULES: () */ 
init_latch_label_5:			/* LN: 113 | CYCLE: 0 | RULES: () */ 
for_end_4:			/* LN: 105 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20c)			# LN: 92 | 
	a0 = xmem[i0]			# LN: 92 | 
	uhalfword(a1) = (0x1)			# LN: 92 | 
	a0 = a0 + a1			# LN: 92 | 
	i0 = i7 - (0x20c)			# LN: 92 | 
	xmem[i0] = a0h			# LN: 92 | 
	jmp (for_1)			# LN: 92 | 
cline_92_1:			/* LN: 118 | CYCLE: 0 | RULES: () */ 
for_end_1:			/* LN: 92 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 118 | 
	i0 = xmem[i0]			# LN: 118 | 
	call (_cl_wavread_close)			# LN: 118 | 
cline_118_0:			/* LN: 119 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 119 | 
	i0 = xmem[i0]			# LN: 119 | 
	call (_cl_wavwrite_close)			# LN: 119 | 
cline_119_0:			/* LN: 122 | CYCLE: 0 | RULES: () */ 
	a0 = 0			# LN: 122 | 
	jmp (__epilogue_242)			# LN: 122 | 
cline_122_0:			/* LN: 123 | CYCLE: 0 | RULES: () */ 
__epilogue_242:			/* LN: 123 | CYCLE: 0 | RULES: () */ 
	i7 = i7 - (0x212)			# LN: 123 | 
	i7 -= 1			# LN: 123 | 
	i6 = xmem[i7]; i7 -= 1			# LN: 123, 123 | 
	i3 = xmem[i7]; i7 -= 1			# LN: 123, 123 | 
	i2 = xmem[i7]; i7 -= 1			# LN: 123, 123 | 
	ret			# LN: 123 | 
