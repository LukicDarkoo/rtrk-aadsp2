"MultichannelSurroundSoundSystem//Debug//Model_0.exe" resources/streams/WhiteNoise2.wav output/speech_0.wav -7 -2 4
"MultichannelSurroundSoundSystem//Debug//Model_1.exe" resources/streams/WhiteNoise2.wav output/speech_1.wav -7 -2 4
"MultichannelSurroundSoundSystem//Debug//Model_2.exe" resources/streams/WhiteNoise2.wav output/speech_2.wav -7 -2 4
"resources//tools//PCMCompare.exe" output//speech_0.wav output//speech_1.wav 2> output//speech_comp_0_1.txt
"resources//tools//PCMCompare.exe" output//speech_1.wav output//speech_2.wav 2> output//speech_comp_1_2.txt
"resources//tools//PCMCompare.exe" output//speech_2.wav output//speech_3.wav 2> output//speech_comp_2_3.txt
goto :EOF


for /l %%x in (0, 1, 0) do (
	"MultichannelSurroundSoundSystem//Debug//Model_0.exe" resources/streams/WhiteNoise2.wav output/speech_0.wav -7 -2 %%x
	"MultichannelSurroundSoundSystem//Debug//Model_1.exe" resources/streams/WhiteNoise2.wav output/speech_1.wav -7 -2 %%x
	"MultichannelSurroundSoundSystem//Debug//Model_2.exe" resources/streams/WhiteNoise2.wav output/speech_2.wav -7 -2 %%x

	
	powershell -Command "(gc MultichannelSurroundSoundSystemModel3/SimulatorConfigurationGeneric.sbr) -replace '{InputGain}', '-7' | Out-File SimulatorConfiguration.sbr"
	powershell -Command "(gc SimulatorConfiguration.sbr) -replace '{HeadroomGain}', '-2' | Out-File SimulatorConfiguration.sbr"
	powershell -Command "(gc SimulatorConfiguration.sbr) -replace '{Mode}', %%x | Out-File SimulatorConfiguration.sbr"
	powershell -Command "(gc SimulatorConfiguration.sbr) -replace '{InputFile}', 'WhiteNoise2.wav' | Out-File -Encoding utf8 SimulatorConfiguration.sbr"
	"C:\CirrusDSP\bin\cmdline_simulator.exe" -project SimulatorConfiguration.sbr -max_cycles 100000
	
	
	"resources//tools//PCMCompare.exe" output//speech_0.wav output//speech_1.wav 2> output//speech_comp_0_1.txt
	"resources//tools//PCMCompare.exe" output//speech_1.wav output//speech_2.wav 2> output//speech_comp_1_2.txt
	"resources//tools//PCMCompare.exe" output//speech_1.wav output//speech_3.wav 2> output//speech_comp_2_3.txt
)
