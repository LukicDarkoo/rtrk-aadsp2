## Test 0
Description: Initial port from Visual Studio to CLIDE  

Memory:
	main_GEN_0000, address 0111 ,length 0001, class     X_OVLY
	main_GEN_0001, address 0112 ,length 0001, class     X_OVLY
	main_GEN_0002, address 006F ,length 0060, class     X_OVLY
	main_GEN_0003, address 00CF ,length 0021, class     X_OVLY
	main_GEN_0004, address 00F0 ,length 0021, class     X_OVLY
	main_GEN_0005, address 01AF ,length 00F2, class  CODE_OVLY

### Input (-7dB, -2dB, SURROUND_MODE_3_2_0)
Cycles for `Surround_Process()`: 2850

### Input (-7dB, -2dB, SURROUND_MODE_2_0_0)
Cycles for `Surround_Process()`: 1590



## Test 1
Description: Move `sampleBuffer` to Y memory
Memory:
	main_GEN_0000, address 00B1 ,length 0001, class     X_OVLY
	main_GEN_0001, address 00B2 ,length 0001, class     X_OVLY
	main_GEN_0002, address 0001 ,length 0060, class     Y_OVLY
	main_GEN_0003, address 006F ,length 0021, class     X_OVLY
	main_GEN_0004, address 0090 ,length 0021, class     X_OVLY
	main_GEN_0005, address 01B7 ,length 00F2, class  CODE_OVLY
	
### Input (-7dB, -2dB, SURROUND_MODE_3_2_0)
Cycles for `Surround_Process()`: 2850

### Input (-7dB, -2dB, SURROUND_MODE_2_0_0)
Cycles for `Surround_Process()`: 1590



## Test 2
