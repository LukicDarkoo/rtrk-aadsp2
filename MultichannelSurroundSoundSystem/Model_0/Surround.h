#include "common.h"

#define SURROUND_CIRC_BUFF_SIZE 100

typedef enum _SurroundMode {
	SURROUND_MODE_2_0_0 = 0,
	SURROUND_MODE_2_0_1,
	SURROUND_MODE_0_2_0,
	SURROUND_MODE_0_2_1,
	SURROUND_MODE_3_2_0
} SurroundMode;

typedef struct _Surround {
	// Buffers
	double* inputLeftBuffer;
	double* inputRightBuffer;
	double* outputLsBuffer;
	double* outputLeftBuffer;
	double* outputCenterBuffer;
	double* outputRightBuffer;
	double* outputRsBuffer;
	double* outputLfeBuffer;

	// Gains
	double inputLeftGain;
	double inputRightGain;
	double headroomGain;
	double outputLsGain;
	double outputLeftGain;
	double outputRightGain;
	double outputRsGain;

	// Mode
	bool enableMiddleCenter;
	bool enableLsRs;
	bool enableCenter;
	bool enableLeftRight;
	bool enableLfe;

	// Delays
	int leftDelaySamples;
	int rightDelaySamples;

	// Circular buffers
	double leftCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	int leftCircularBufferWriteIndex;
	int leftCircularBufferReadIndex;
	double rightCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	int rightCircularBufferWriteIndex;
	int rightCircularBufferReadIndex;
} Surround;

void Surround_Init(Surround* surroundState, int inputGain, int headroomGain, SurroundMode mode);

void Surround_Process(
	Surround* surroundState, 
	int length,
	double* inputLeftBuffer, 
	double* inputRightBuffer,
	double* outputLsBuffer,
	double* outputLeftBuffer,
	double* outputCenterBuffer,
	double* outputRightBuffer,
	double* outputRsBuffer,
	double* outputLfeBuffer
	);

void Surround_SetMode(Surround* surround, SurroundMode mode);