#include "Surround.h"
#include <string.h>
#include <math.h>

static inline int msToDelay(int ms) { return (ms / ((1.0 / SAMPLE_RATE) * 1000)); }
static inline double dbToGain(double db) { return (pow(10, db / 20.0)); }

void Surround_Init(Surround* surround, int inputGain, int headroomGain, SurroundMode mode) {
	surround->inputLeftGain = dbToGain(inputGain);		// -7dB
	surround->inputRightGain = dbToGain(inputGain);	// -7dB
	surround->headroomGain = dbToGain(headroomGain);		// -2dB
	surround->outputLsGain = dbToGain(-6);		// -6dB
	surround->outputLeftGain = dbToGain(-1);	// -1dB
	surround->outputRightGain = dbToGain(-1);	// -1dB
	surround->outputRsGain = dbToGain(-6);		// -6dB
	surround->leftDelaySamples = msToDelay(1);	// 1ms
	surround->rightDelaySamples = msToDelay(1);	// 1ms

	surround->leftCircularBufferReadIndex = 0;
	surround->rightCircularBufferReadIndex = 0;
	surround->leftCircularBufferWriteIndex = surround->leftDelaySamples - 1;
	surround->rightCircularBufferWriteIndex = surround->rightDelaySamples - 1;

	memset(surround->leftCircularBuffer, 0, sizeof(double) * surround->leftDelaySamples);
	memset(surround->rightCircularBuffer, 0, sizeof(double) * surround->rightDelaySamples);

	Surround_SetMode(surround, mode);
}


void Surround_SetMode(Surround* surround, SurroundMode mode) {
	switch(mode) {
	case SURROUND_MODE_2_0_0:
		surround->enableMiddleCenter = true;
		surround->enableLsRs = false;
		surround->enableCenter = false;
		surround->enableLeftRight = true;
		surround->enableLfe = false;
		break;

	case SURROUND_MODE_2_0_1:
		surround->enableMiddleCenter = true;
		surround->enableLsRs = false;
		surround->enableCenter = false;
		surround->enableLeftRight = true;
		surround->enableLfe = true;
		break;

	case SURROUND_MODE_0_2_0:
		surround->enableMiddleCenter = false;
		surround->enableLsRs = true;
		surround->enableCenter = false;
		surround->enableLeftRight = false;
		surround->enableLfe = false;
		break;

	case SURROUND_MODE_0_2_1:
		surround->enableMiddleCenter = true;
		surround->enableLsRs = true;
		surround->enableCenter = false;
		surround->enableLeftRight = false;
		surround->enableLfe = true;
		break;

	case SURROUND_MODE_3_2_0:
		surround->enableMiddleCenter = true;
		surround->enableLsRs = true;
		surround->enableCenter = true;
		surround->enableLeftRight = true;
		surround->enableLfe = false;
		break;
	}
}

void Surround_Process(
	Surround* surround, 
	int length,
	double* inputLeftBuffer, 
	double* inputRightBuffer,
	double* outputLsBuffer,
	double* outputLeftBuffer,
	double* outputCenterBuffer,
	double* outputRightBuffer,
	double* outputRsBuffer,
	double* outputLfBuffer
	) {
		for (int i = 0; i < length; i++) {
			double middleStageCenter;
			double middleStageLeft;
			double middleStageRight;
			double inputStageLeft;
			double inputStageRight;

			// Input stage
			inputStageLeft = inputLeftBuffer[i] * surround->inputLeftGain;
			inputStageRight = inputRightBuffer[i] * surround->inputRightGain;
			
			
			if (surround->enableMiddleCenter == true) {
				// Middle stage
				middleStageCenter = (inputStageLeft + inputStageRight) * surround->headroomGain;


				// Output stage
				if (surround->enableLeftRight == true) {
					outputLeftBuffer[i] = middleStageCenter * surround->outputLeftGain;
					outputRightBuffer[i] = middleStageCenter * surround->outputRightGain;
				}

				if (surround->enableCenter == true) {
					outputCenterBuffer[i] = middleStageCenter;
				}
				
				if (surround->enableLfe == true) { 
					outputLfBuffer[i] = middleStageCenter;
				}
			}

			if (surround->enableLsRs == true) {
				// Middle stage
				middleStageLeft = (surround->leftCircularBuffer[surround->leftCircularBufferReadIndex] * surround->outputLsGain);
				middleStageRight = (surround->rightCircularBuffer[surround->rightCircularBufferReadIndex] * surround->outputRsGain);

				surround->leftCircularBuffer[surround->leftCircularBufferWriteIndex] = middleStageLeft + inputStageLeft;
				surround->rightCircularBuffer[surround->rightCircularBufferWriteIndex] = middleStageRight + inputStageRight;

				surround->leftCircularBufferWriteIndex = (surround->leftCircularBufferWriteIndex + 1) % surround->leftDelaySamples;
				surround->rightCircularBufferWriteIndex = (surround->rightCircularBufferWriteIndex + 1) % surround->rightDelaySamples;
				surround->leftCircularBufferReadIndex = (surround->leftCircularBufferReadIndex + 1) % surround->leftDelaySamples;
				surround->rightCircularBufferReadIndex = (surround->rightCircularBufferReadIndex + 1) % surround->rightDelaySamples;


				// Output stage
				outputLsBuffer[i] = middleStageLeft;
				outputRsBuffer[i] = middleStageRight;

				if (surround->enableLeftRight == false) {
					outputLeftBuffer[i] = 0;
					outputRightBuffer[i] = 0;
				}
			}
		}
}