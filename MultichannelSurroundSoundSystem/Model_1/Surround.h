#include "common.h"

#define SURROUND_DELAY_MS 1
// #define SURROUND_DELAY_SAMPLES (int)(SURROUND_DELAY_MS / ((1.0 / SAMPLE_RATE) * 1000))
#define SURROUND_DELAY_SAMPLES 44
#define SURROUND_CIRC_BUFF_SIZE SURROUND_DELAY_SAMPLES

typedef enum _SurroundMode {
	SURROUND_MODE_2_0_0 = 0,
	SURROUND_MODE_2_0_1,
	SURROUND_MODE_0_2_0,
	SURROUND_MODE_0_2_1,
	SURROUND_MODE_3_2_0
} SurroundMode;

typedef struct _Surround {
	// Buffers
	double* inputLeftBuffer;
	double* inputRightBuffer;
	double* outputLsBuffer;
	double* outputLeftBuffer;
	double* outputCenterBuffer;
	double* outputRightBuffer;
	double* outputRsBuffer;
	double* outputLfeBuffer;

	// Gains
	double inputLeftGain;
	double inputRightGain;
	double headroomGain;
	double outputLsGain;
	double outputLeftGain;
	double outputRightGain;
	double outputRsGain;

	// Mode
	bool enableMiddleCenter;
	bool enableLsRs;
	bool enableCenter;
	bool enableLeftRight;
	bool enableLfe;

	// Circular buffers
	double leftCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	double rightCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	int circularBufferReadIndex;
	int circularBufferWriteIndex;
} Surround;

void Surround_Init(
	int inputGain, int headroomGain, SurroundMode mode,
	double* inputLeftBuffer, 
	double* inputRightBuffer,
	double* outputLsBuffer,
	double* outputLeftBuffer,
	double* outputCenterBuffer,
	double* outputRightBuffer,
	double* outputRsBuffer,
	double* outputLfeBuffer
);

void Surround_Process();

void Surround_SetMode(SurroundMode mode);