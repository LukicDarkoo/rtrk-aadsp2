#include "Surround.h"
#include <string.h>
#include <math.h>

static inline double dbToGain(double db) { return (pow(10, db / 20.0)); }
static Surround surround;

void Surround_Init(
	int inputGain, int headroomGain, SurroundMode mode,
	double* inputLeftBuffer, 
	double* inputRightBuffer,
	double* outputLsBuffer,
	double* outputLeftBuffer,
	double* outputCenterBuffer,
	double* outputRightBuffer,
	double* outputRsBuffer,
	double* outputLfeBuffer
) {
	surround.inputLeftBuffer = inputLeftBuffer;
	surround.inputRightBuffer = inputRightBuffer;
	surround.outputLsBuffer = outputLsBuffer;
	surround.outputLeftBuffer = outputLeftBuffer;
	surround.outputCenterBuffer = outputCenterBuffer;
	surround.outputRightBuffer = outputRightBuffer;
	surround.outputRsBuffer = outputRsBuffer;
	surround.outputLfeBuffer = outputLfeBuffer;

	surround.inputLeftGain = dbToGain(inputGain);		// -7dB
	surround.inputRightGain = dbToGain(inputGain);	// -7dB
	surround.headroomGain = dbToGain(headroomGain);		// -2dB
	surround.outputLsGain = dbToGain(-6);		// -6dB
	surround.outputLeftGain = dbToGain(-1);	// -1dB
	surround.outputRightGain = dbToGain(-1);	// -1dB
	surround.outputRsGain = dbToGain(-6);		// -6dB

	surround.circularBufferReadIndex = 0;
	surround.circularBufferWriteIndex = SURROUND_DELAY_SAMPLES - 1;

	Surround_SetMode(mode);
}


void Surround_SetMode(SurroundMode mode) {
	switch(mode) {
	case SURROUND_MODE_2_0_0:
		surround.enableMiddleCenter = true;
		surround.enableLsRs = false;
		surround.enableCenter = false;
		surround.enableLeftRight = true;
		surround.enableLfe = false;
		break;

	case SURROUND_MODE_2_0_1:
		surround.enableMiddleCenter = true;
		surround.enableLsRs = false;
		surround.enableCenter = false;
		surround.enableLeftRight = true;
		surround.enableLfe = true;
		break;

	case SURROUND_MODE_0_2_0:
		surround.enableMiddleCenter = false;
		surround.enableLsRs = true;
		surround.enableCenter = false;
		surround.enableLeftRight = false;
		surround.enableLfe = false;
		break;

	case SURROUND_MODE_0_2_1:
		surround.enableMiddleCenter = true;
		surround.enableLsRs = true;
		surround.enableCenter = false;
		surround.enableLeftRight = false;
		surround.enableLfe = true;
		break;

	case SURROUND_MODE_3_2_0:
		surround.enableMiddleCenter = true;
		surround.enableLsRs = true;
		surround.enableCenter = true;
		surround.enableLeftRight = true;
		surround.enableLfe = false;
		break;
	}
}

void Surround_Process() {
		double middleStageCenter;
		double middleStageLeft;
		double middleStageRight;
		double inputStageLeft;
		double inputStageRight;

		double *inputLeftBuffer = surround.inputLeftBuffer;
		double *inputRightBuffer = surround.inputRightBuffer;
		double *outputLsBuffer = surround.outputLsBuffer;
		double *outputLeftBuffer = surround.outputLeftBuffer;
		double *outputCenterBuffer = surround.outputCenterBuffer;
		double *outputLfeBuffer = surround.outputLfeBuffer;
		double *outputRightBuffer = surround.outputRightBuffer;
		double *outputRsBuffer = surround.outputRsBuffer;


		for (int i = 0; i < BLOCK_SIZE; i++) {
			// Input stage
			inputStageLeft = *inputLeftBuffer * surround.inputLeftGain;
			inputStageRight = *inputRightBuffer * surround.inputRightGain;
			
			
			if (surround.enableMiddleCenter == 1) {
				// Middle stage
				middleStageCenter = (inputStageLeft + inputStageRight) * surround.headroomGain;


				// Output stage
				if (surround.enableLeftRight == 1) {
					*outputLeftBuffer++ = middleStageCenter * surround.outputLeftGain;
					*outputRightBuffer++ = middleStageCenter * surround.outputRightGain;
				}

				if (surround.enableCenter == 1) {
					*outputCenterBuffer++ = middleStageCenter;
				} 
				
				if (surround.enableLfe == 1) { 
					*outputLfeBuffer++ = middleStageCenter;
				}
			}

			if (surround.enableLsRs == 1) {
				// Middle stage
				middleStageLeft = (surround.leftCircularBuffer[surround.circularBufferReadIndex] * surround.outputLsGain);
				middleStageRight = (surround.rightCircularBuffer[surround.circularBufferReadIndex] * surround.outputRsGain);

				surround.leftCircularBuffer[surround.circularBufferWriteIndex] = middleStageLeft + inputStageLeft;
				surround.rightCircularBuffer[surround.circularBufferWriteIndex] = middleStageRight + inputStageRight;

				surround.circularBufferWriteIndex = (surround.circularBufferWriteIndex + 1) % SURROUND_DELAY_SAMPLES;
				surround.circularBufferReadIndex = (surround.circularBufferReadIndex + 1) % SURROUND_DELAY_SAMPLES;

				// Output stage
				*outputLsBuffer++ = middleStageLeft;
				*outputRsBuffer++ = middleStageRight;
			}

			if (surround.enableLeftRight == 0) {
				*outputLeftBuffer++ = 0;
				*outputRightBuffer++ = 0;
			}

			inputLeftBuffer++;
			inputRightBuffer++;
		}
}