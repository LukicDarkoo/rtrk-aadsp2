#include "common.h"

// #define SURROUND_DELAY_MS 1
// #define SURROUND_DELAY_SAMPLES (int)(SURROUND_DELAY_MS / ((1.0 / SAMPLE_RATE) * 1000))
#define SURROUND_DELAY_SAMPLES 44
#define SURROUND_CIRC_BUFF_SIZE SURROUND_DELAY_SAMPLES

typedef enum _SurroundMode {
	SURROUND_MODE_2_0_0 = 0,
	SURROUND_MODE_2_0_1,
	SURROUND_MODE_0_2_0,
	SURROUND_MODE_0_2_1,
	SURROUND_MODE_3_2_0
} SurroundMode;

typedef struct _Surround {
	// Buffers
	DSPfract* inputLeftBuffer;
	DSPfract* inputRightBuffer;
	DSPfract* outputLsBuffer;
	DSPfract* outputLeftBuffer;
	DSPfract* outputCenterBuffer;
	DSPfract* outputRightBuffer;
	DSPfract* outputRsBuffer;
	DSPfract* outputLfeBuffer;

	// Gains
	DSPfract inputLeftGain;
	DSPfract inputRightGain;
	DSPfract headroomGain;
	DSPfract outputLsGain;
	DSPfract outputLeftGain;
	DSPfract outputRightGain;
	DSPfract outputRsGain;

	// Mode
	DSPint enableMiddleCenter;
	DSPint enableLsRs;
	DSPint enableCenter;
	DSPint enableLeftRight;
	DSPint enableLfe;

	// Circular buffers
	DSPfract leftCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	DSPfract rightCircularBuffer[SURROUND_CIRC_BUFF_SIZE];
	DSPint circularBufferReadIndex;
	DSPint circularBufferWriteIndex;
} Surround;

void Surround_Init(
	DSPint inputGain, DSPint headroomGain, SurroundMode mode,
	DSPfract* inputLeftBuffer, 
	DSPfract* inputRightBuffer,
	DSPfract* outputLsBuffer,
	DSPfract* outputLeftBuffer,
	DSPfract* outputCenterBuffer,
	DSPfract* outputRightBuffer,
	DSPfract* outputRsBuffer,
	DSPfract* outputLfeBuffer
);

void Surround_Process();

void Surround_SetMode(SurroundMode mode);