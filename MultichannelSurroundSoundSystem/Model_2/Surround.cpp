#include "Surround.h"
#include <string.h>
#include <math.h>

static inline DSPfract dbToGain(DSPint db) { return FRACT_NUM(pow(10, db / 20.0)); }
static Surround surround;

void Surround_Init(
	int inputGain, int headroomGain, SurroundMode mode,
	DSPfract* inputLeftBuffer, 
	DSPfract* inputRightBuffer,
	DSPfract* outputLsBuffer,
	DSPfract* outputLeftBuffer,
	DSPfract* outputCenterBuffer,
	DSPfract* outputRightBuffer,
	DSPfract* outputRsBuffer,
	DSPfract* outputLfeBuffer
) {
	surround.inputLeftBuffer = inputLeftBuffer;
	surround.inputRightBuffer = inputRightBuffer;
	surround.outputLsBuffer = outputLsBuffer;
	surround.outputLeftBuffer = outputLeftBuffer;
	surround.outputCenterBuffer = outputCenterBuffer;
	surround.outputRightBuffer = outputRightBuffer;
	surround.outputRsBuffer = outputRsBuffer;
	surround.outputLfeBuffer = outputLfeBuffer;

	surround.inputLeftGain = dbToGain(inputGain);		// -7dB
	surround.inputRightGain = dbToGain(inputGain);	// -7dB
	surround.headroomGain = dbToGain(headroomGain);		// -2dB
	surround.outputLsGain = dbToGain(-6);		// -6dB
	surround.outputLeftGain = dbToGain(-1);	// -1dB
	surround.outputRightGain = dbToGain(-1);	// -1dB
	surround.outputRsGain = dbToGain(-6);		// -6dB

	surround.circularBufferReadIndex = 0;
	surround.circularBufferWriteIndex = SURROUND_DELAY_SAMPLES - 1;

	for (DSPint i = 0; i < SURROUND_CIRC_BUFF_SIZE; i++) {
		surround.leftCircularBuffer[i] = FRACT_NUM(0.0);
		surround.rightCircularBuffer[i] = FRACT_NUM(0.0);
	}

	Surround_SetMode(mode);
}


void Surround_SetMode(SurroundMode mode) {
	switch(mode) {
	case SURROUND_MODE_2_0_0:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 0;
		surround.enableCenter = 0;
		surround.enableLeftRight = 1;
		surround.enableLfe = 0;
		break;

	case SURROUND_MODE_2_0_1:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 0;
		surround.enableCenter = 0;
		surround.enableLeftRight = 1;
		surround.enableLfe = 1;
		break;

	case SURROUND_MODE_0_2_0:
		surround.enableMiddleCenter = 0;
		surround.enableLsRs = 1;
		surround.enableCenter = 0;
		surround.enableLeftRight = 0;
		surround.enableLfe = 0;
		break;

	case SURROUND_MODE_0_2_1:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 1;
		surround.enableCenter = 0;
		surround.enableLeftRight = 0;
		surround.enableLfe = 1;
		break;

	case SURROUND_MODE_3_2_0:
		surround.enableMiddleCenter = 1;
		surround.enableLsRs = 1;
		surround.enableCenter = 1;
		surround.enableLeftRight = 1;
		surround.enableLfe = 0;
		break;
	}
}

void Surround_Process() {
		DSPfract middleStageCenter;
		DSPfract middleStageLeft;
		DSPfract middleStageRight;
		DSPfract inputStageLeft;
		DSPfract inputStageRight;

		DSPfract *inputLeftBuffer = surround.inputLeftBuffer;
		DSPfract *inputRightBuffer = surround.inputRightBuffer;
		DSPfract *outputLsBuffer = surround.outputLsBuffer;
		DSPfract *outputLeftBuffer = surround.outputLeftBuffer;
		DSPfract *outputCenterBuffer = surround.outputCenterBuffer;
		DSPfract *outputLfeBuffer = surround.outputLfeBuffer;
		DSPfract *outputRightBuffer = surround.outputRightBuffer;
		DSPfract *outputRsBuffer = surround.outputRsBuffer;


		for (int i = 0; i < BLOCK_SIZE; i++) {
			// Input stage
			inputStageLeft = *inputLeftBuffer * surround.inputLeftGain;
			inputStageRight = *inputRightBuffer * surround.inputRightGain;
			
			
			if (surround.enableMiddleCenter == 1) {
				// Middle stage
				middleStageCenter = (inputStageLeft + inputStageRight) * surround.headroomGain;


				// Output stage
				if (surround.enableLeftRight == 1) {
					*outputLeftBuffer++ = middleStageCenter * surround.outputLeftGain;
					*outputRightBuffer++ = middleStageCenter * surround.outputRightGain;
				}

				if (surround.enableCenter == 1) {
					*outputCenterBuffer++ = middleStageCenter;
				}
				
				if (surround.enableLfe == 1) { 
					*outputLfeBuffer++ = middleStageCenter;
				}
			}

			if (surround.enableLsRs == 1) {
				// Middle stage
				middleStageLeft = (surround.leftCircularBuffer[surround.circularBufferReadIndex] * surround.outputLsGain);
				middleStageRight = (surround.rightCircularBuffer[surround.circularBufferReadIndex] * surround.outputRsGain);

				surround.leftCircularBuffer[surround.circularBufferWriteIndex] = middleStageLeft + inputStageLeft;
				surround.rightCircularBuffer[surround.circularBufferWriteIndex] = middleStageRight + inputStageRight;

				surround.circularBufferWriteIndex = (surround.circularBufferWriteIndex + 1) % SURROUND_DELAY_SAMPLES;
				surround.circularBufferReadIndex = (surround.circularBufferReadIndex + 1) % SURROUND_DELAY_SAMPLES;

				// Output stage
				*outputLsBuffer++ = middleStageLeft;
				*outputRsBuffer++ = middleStageRight;
			}

			if (surround.enableLeftRight == 0) {
				*outputLeftBuffer++ = FRACT_NUM(0.0);
				*outputRightBuffer++ = FRACT_NUM(0.0);
			}

			inputLeftBuffer++;
			inputRightBuffer++;
		}
}